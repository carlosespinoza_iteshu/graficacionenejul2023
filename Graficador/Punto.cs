﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graficador
{
    public class Punto
    {
        public double X { get; set; }
        public double Xajustado { get; set; }
        public double Y { get; set; }
        public double Yajustado { get; set; }
    }
}
