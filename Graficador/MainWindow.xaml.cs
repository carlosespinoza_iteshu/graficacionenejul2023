﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graficador
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnGraficar_Click(object sender, RoutedEventArgs e)
        {
            //limpiar mi lienzo
            gridLienzo.Children.Clear();
            //dibujar la linea central del eje X
            Line ejeX = new Line();
            ejeX.X1 = 0;
            ejeX.Y1 = gridLienzo.ActualHeight / 2;
            ejeX.X2 = gridLienzo.ActualWidth;
            ejeX.Y2 = gridLienzo.ActualHeight / 2;
            ejeX.Stroke = new SolidColorBrush(Colors.Black);
            ejeX.StrokeThickness = 1;
            gridLienzo.Children.Add(ejeX);

            //Calcular los puntos
            //X-> -1 a 1
            List<Punto> puntos = new List<Punto>();
            for (double x = double.Parse(txbXInf.Text); x <= double.Parse(txbXSup.Text); x += double.Parse(txbIncrementoX.Text))
            {
                puntos.Add(new Punto()
                {
                    X = x,
                    Xajustado=AjustarX(x),
                    Y = Math.Sin(x),
                    Yajustado=AjustarY(Math.Sin(x))
                });
            }




            for (int i = 1; i < puntos.Count; i++)
            {
                
                TrazarLinea(puntos[i - 1].X, puntos[i - 1].Yajustado, puntos[i].X, puntos[i].Yajustado);
            }
            
        }

        private double AjustarX(double x)
        {
            double inf = double.Parse(txbXInf.Text);
            double sup = double.Parse(txbXSup.Text);
            double cantidad=(Math.Abs(sup)+Math.Abs(inf))/double.Parse(txbIncrementoX.Text);
            return cantidad;
        }

        private double AjustarY(double y)
        {
            double ajustado = 0;
            double mitad = gridLienzo.ActualHeight/2;
            double alto = mitad * 2;
            if (y < 0)
            {
                //negativo
                ajustado=mitad+(mitad*Math.Abs(y));
            }
            else
            {
                ajustado = ((mitad / 100) * y);
            }
            return ajustado;
        }

        public void TrazarLinea(double x1, double y1, double x2, double y2)
        {
            Line linea = new Line();
            linea.X1 = x1;
            linea.Y1 = y1;
            linea.X2 = x2;
            linea.Y2 = y2;
            linea.Stroke = new SolidColorBrush(Colors.Blue);
            linea.StrokeThickness = 1;
            gridLienzo.Children.Add(linea);
        }
    }
}
