﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Reto01
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Punto> puntos= new List<Punto>();
        int n = 10;
        Random r = new Random();
        public MainWindow()
        {
            InitializeComponent();
           // Pintar(int.Parse(txbCaja.text));
        }

        private void Pintar(int n)
        {
            puntos.Clear();
            for (int i = 0; i < n; i++)
            {
                puntos.Add(new Punto()
                {
                    Numero = i + 1,
                    X = r.Next(0,100),
                    Y= r.Next(0,70)
                });
            }
            foreach (var item in puntos)
            {
                Label label = new Label();
                label.Content = item.Numero.ToString();
                label.FontSize = 12;
                TranslateTransform translate = new TranslateTransform(item.X, item.Y);
                label.RenderTransform= translate;
                //Lienzo.Children.Add(label);
            }
        }
    }
}
