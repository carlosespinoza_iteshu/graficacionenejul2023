﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoGraficacion
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            for (int i = 0; i < this.Width; i+=50)
            {
                Label label = new Label();
                label.Content= i.ToString();
                label.FontSize= 10;
                TranslateTransform translate=new TranslateTransform(i,0);
                label.RenderTransform = translate;
                gridLinezo.Children.Add(label);
            }

            for (int i = 0; i < this.Height; i += 50)
            {
                Label label = new Label();
                label.Content = i.ToString();
                label.FontSize = 10;
                TranslateTransform translate = new TranslateTransform(0, i);
                label.RenderTransform = translate;
                gridLinezo.Children.Add(label);
            }

        }
    }
}
