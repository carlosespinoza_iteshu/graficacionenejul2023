﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Demo4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Rectangle rectangulo;
        Ellipse centro;
        TransformGroup group;
        public MainWindow()
        {
            InitializeComponent();
            group = new TransformGroup();
            rectangulo = new Rectangle();
            rectangulo.Height = 150;
            rectangulo.Width = 300;
            SolidColorBrush brochaRoja = new SolidColorBrush();
            brochaRoja.Color = Colors.Red;
            SolidColorBrush brochaVerde = new SolidColorBrush();
            brochaVerde.Color = Colors.Green;
            rectangulo.Fill = brochaVerde; //relleno
            rectangulo.Stroke= brochaRoja; //contorno
            rectangulo.StrokeThickness = 4;
            Lienzo.Children.Add(rectangulo);

            centro = new Ellipse();
            centro.Height = 3;
            centro.Width = 3;
            SolidColorBrush brochaNegra=new SolidColorBrush();
            brochaNegra.Color = Colors.Black;
            centro.Fill=brochaNegra;
            centro.Stroke= brochaNegra;
            centro.StrokeThickness = 1;
            Lienzo.Children.Add(centro);
        }

        private void btnRotar_Click(object sender, RoutedEventArgs e)
        {
            int x = int.Parse(txbOrigenX.Text);
            int y = int.Parse(txbOrigenY.Text);
            int angulo = int.Parse(txbAngulo.Text);
            MoverCentro(x,y);
            RotateTransform rotacion=new RotateTransform(angulo,x,y);
            group.Children.Add(rotacion);
            AplicaTransformacion();
        }
        private void AplicaTransformacion()
        {
            rectangulo.RenderTransform = group;
        }

        private void MoverCentro(int x, int y)
        {
            TranslateTransform traslacion=new TranslateTransform(
                -(rectangulo.ActualWidth/2)+ x,
                -(rectangulo.ActualHeight/2)+ y);
            centro.RenderTransform= traslacion;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int x = int.Parse(txbOrigenX.Text);
            int y = int.Parse(txbOrigenY.Text);
            int angulo =int.Parse( 
                Math.Round(e.NewValue,0).ToString());
            MoverCentro(x, y);
            RotateTransform rotacion = new RotateTransform(angulo, x, y);
            group.Children.Add(rotacion);
            AplicaTransformacion();
            
        }

        private void sliderEscala_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
            ScaleTransform escala = new ScaleTransform(e.NewValue, e.NewValue);
            if (rectangulo != null)
            {
                group.Children.Add(escala);
                AplicaTransformacion();
            }
        }

        private void sliderSesgo_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SkewTransform sesgo = new SkewTransform(e.NewValue, 0);
            group.Children.Add(sesgo);
            AplicaTransformacion();
        }
    }
}
